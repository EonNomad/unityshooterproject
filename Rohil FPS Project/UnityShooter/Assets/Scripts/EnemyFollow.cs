﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour {
	Transform target;

	public float moveSpeed;
	Vector3 enemyDir;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
	}
		
	
	// Update is called once per frame
	void Update () {
		//move towards
		float step = moveSpeed * Time.deltaTime;

		//transform.position = Vector3.MoveTowards(transform.position, target.position, step);
		//enemyDir = Vector3.MoveTowards(transform.position, target.position, step);
		transform.position += transform.forward * step;
		//gameObject.GetComponent<Rigidbody> ().MovePosition (transform.position + -enemyDir * step);

		//rotate towards
		Vector3 targetDir = target.position - transform.position;
		//float step = moveSpeed * Time.deltaTime;
		Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
		Debug.DrawRay(transform.position, newDir, Color.red);
		transform.rotation = Quaternion.LookRotation(newDir);
	}
}
