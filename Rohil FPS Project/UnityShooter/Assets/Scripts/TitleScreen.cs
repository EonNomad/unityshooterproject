﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour {

    public GameObject titleCanvas;
    public EventSystem titlescreenES;
    public Button firstButton;

    // Use this for initialization
    void Start () {

        titlescreenES.SetSelectedGameObject(null);
        titlescreenES.SetSelectedGameObject(firstButton.gameObject);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartGame()
    {
        //May change when official name given to scene
        //SceneManager.LoadScene("DialogueTest");

        Scene currentScene = SceneManager.GetActiveScene();
        //Load scene that is after current scene by adding 1 to build index
        SceneManager.LoadScene(currentScene.buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
