﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuGO;
    public GameObject ReticleGO;
    public GameObject Player;
    bool isPauseMenuUp;

    public GameObject titleCanvas;
    public EventSystem titlescreenES;
    public Button firstButton;
    string Pause;

    // Use this for initialization
    void Start()
    {
        //PauseMenuGO.SetActive(false);
        isPauseMenuUp = false;

        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            Pause = "Windows Pause";
        }

        if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            Pause = "Mac Pause";
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(Pause))
        {
            Debug.Log("Pausing game");
            if (!isPauseMenuUp)
            {
                PauseGame();

            }else if (isPauseMenuUp)
            {
                ContinueGame();
            }

        }
    }

    void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenuGO.SetActive(true);
        ReticleGO.SetActive(false);
        isPauseMenuUp = true;

        titlescreenES.SetSelectedGameObject(null);
        titlescreenES.SetSelectedGameObject(firstButton.gameObject);
        Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
        Debug.Log("isPause working?");
        //Disable scripts that still work while timescale is set to 0
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        PauseMenuGO.SetActive(false);
        ReticleGO.SetActive(true);
        isPauseMenuUp = false;
        Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        //enable the scripts again
    }


    public void ReturnToTitle()
    {
        //Scene currentScene = SceneManager.GetActiveScene();
        //Load scene that is after current scene by adding 1 to build index
        Time.timeScale = 1;
        PauseMenuGO.SetActive(false);
        ReticleGO.SetActive(true);
        isPauseMenuUp = false;
        Player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
        SceneManager.LoadScene("TitleScreen");
    }

}
