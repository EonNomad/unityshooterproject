﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1AI : MonoBehaviour {
    Transform target;

    public float moveSpeed;
    public bool canFollow;
    public bool canEvade;

    public float cantEvadeDur;
    Vector3 leftDir;
    Vector3 rightDir;
    Vector3 randomDirection;

    // Use this for initialization
    void Start () {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        canFollow = true;
        canEvade = true;

        leftDir = new Vector3(-30, 0, 0);
        rightDir = new Vector3(30, 0, 0);
        randomDirection = leftDir;

        StartCoroutine(SwitchDirectionRight());
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (canEvade)
        {
            Vector2 mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Ray ray;
            ray = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.rigidbody == this.gameObject.GetComponent<Rigidbody>())
                {
                    float step = moveSpeed * Time.deltaTime;
                    canFollow = false;
                    //Evade

                    //transform.position = Vector3.left;
                    //Vector3 randomDirection = new Vector3(Random.Range(-100,100), 0, 0);
                    //transform.position = transform.position+ randomDirection;
                    this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + randomDirection *Time.deltaTime);
                    StartCoroutine(EvadeCooldown());
                    Debug.Log("detected boss");
                }
            }
        }

        
        if (canFollow)
        {
            //move towards
            GoToPlayer();
        }
        FacePlayer();
        
    }

    void GoToPlayer()
    {
        float step = moveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    void FacePlayer()
    {
        float step = moveSpeed * Time.deltaTime;

        //rotate towards
        Vector3 targetDir = target.position - transform.position;
        //float step = moveSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    IEnumerator EvadeCooldown()
    {
        yield return new WaitForSeconds(2f);
        canEvade = false;
        canFollow = true;
        yield return new WaitForSeconds(cantEvadeDur);
        canEvade = true;
    }

    IEnumerator SwitchDirectionRight()
    {
        yield return new WaitForSeconds(5f);
        randomDirection = rightDir;
        StartCoroutine(SwitchDirectionLeft());
    }

    IEnumerator SwitchDirectionLeft()
    {
        yield return new WaitForSeconds(5f);
        randomDirection = leftDir;
        StartCoroutine(SwitchDirectionRight());
    }
}
