﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public float myHealth;
    public bool isFinalBoss;

	SpriteRenderer renderer;
	Color originalColor;

	// Use this for initialization
	void Start () {
		renderer = GetComponent<SpriteRenderer> ();
		originalColor = renderer.color;
        //GameObject Parent = this.gameObject.transform.parent.gameObject;
        //Debug.Log("Parent" + Parent);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void GetDamage()
    {
        myHealth-- ;
		this.gameObject.GetComponent<AudioSource> ().Play ();
		StartCoroutine (FlashRed());

        if (myHealth == 0)
        {
            //might add point here by calling another script to unlock door
            //GameObject.Find("Exit Door").GetComponent<UnlockDoorCounter>().enemyDeathIncreaseUnlock();
            //if finalboss
            if (isFinalBoss)
            {
                Destroy(this.transform.parent.gameObject);
            } else if (!isFinalBoss)
            {
                //if there is a parent
                //if (this.gameObject.transform.parent.gameObject != null)
                if (this.gameObject.transform.parent != null)
                {
                    //check if parent is an enemy container
                    //if so, destroy the enemy container
                    if (this.gameObject.transform.parent.gameObject.tag == "Enemy")
                    {
                        Destroy(this.transform.parent.gameObject);
                    }
                    else if (this.gameObject.transform.parent.gameObject.tag != "Enemy")
                    {
                        //if not, destroy self
                        Destroy(this.gameObject);
                    }
                }
                //if no parent
                if (this.gameObject.transform.parent == null)
                {
                    //if no parent, destroy self
                    Destroy(this.gameObject);
                }

            }
 
        }
    }
	IEnumerator FlashRed(){
		renderer.color = Color.red;

		yield return new WaitForSeconds(0.5f);
		renderer.color = originalColor;

	}
}
