﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoController : MonoBehaviour {
    MovieTexture Movie;
    public bool introVid;
    public bool outroVid;

    // Use this for initialization
    void Start () {
        Movie = this.gameObject.GetComponent<Renderer>().material.mainTexture as MovieTexture;
       
        Movie.Play();
	}
	
	// Update is called once per frame
	void Update () {
        if (Movie.isPlaying == false)
        {
            if (introVid)
            {
                Scene currentScene = SceneManager.GetActiveScene();
                //Load scene that is after current scene by adding 1 to build index
                SceneManager.LoadScene(currentScene.buildIndex + 1);
            }
            if (outroVid)
            {
                SceneManager.LoadScene("TitleScreen");
            }
        }
	}
}
