﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour {

	GameObject prefab;
	public float fireRate;

	public bool isFry;
	public bool isSniper;
	public bool isBoss1;
	public bool isBoss2;
    public bool isFat;

    // Use this for initialization
    void Start () {
		if(isFry){
			prefab = Resources.Load("FryProjectile") as GameObject;
		}
		if(isSniper){
			prefab = Resources.Load("SniperProjectile") as GameObject;
		}
        if (isFat||isBoss1)
        {
            prefab = Resources.Load("FatProjectile") as GameObject;
        }
        if (isBoss2)
        {
            prefab = Resources.Load("Boss2Projectile") as GameObject;
        }


        InvokeRepeating("LaunchProjectile", 2.0f, fireRate);
	}


    void LaunchProjectile()
    {
        //Rigidbody instance = Instantiate(projectile);

        //instance.velocity = Random.insideUnitSphere * 5;

        GameObject projectile = Instantiate(prefab) as GameObject;
        if (isFry || isFat || isBoss1|| isBoss2){
			projectile.transform.position = transform.position + this.gameObject.transform.forward * 7;
		}

		if(isSniper){
			projectile.transform.position = transform.position + (this.gameObject.transform.forward * 7) + this.gameObject.transform.up * 2 ;
		}
		Rigidbody rb = projectile.GetComponent<Rigidbody>();
		rb.velocity = this.gameObject.transform.forward * 40;
	}
}
