﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour {

    public float myHealth;
    bool isPushedback;
    public bool canGetHurt;
    public float gracePeriod;

	public GameObject FlashRed;

    // Use this for initialization
    void Start () {
        //isPushedback = false;
        Debug.Log("starting pos is: " + this.gameObject.GetComponent<Rigidbody>().position);
        canGetHurt = true;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        if (isPushedback)
        {
            // this.gameObject.GetComponent<Rigidbody>().AddForce(0,0,-20, ForceMode.VelocityChange);
            // Vector3 playerPos = this.gameObject.GetComponent<Rigidbody>().position;
            // playerPos.z -= 100;
            //this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + (-transform.forward) * Time.deltaTime);
            Vector3 pushedbackDir = new Vector3(0, 0, -150);
            this.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            //this.gameObject.GetComponent<Rigidbody>().AddForce(transform.position + pushedbackDir,ForceMode.VelocityChange);
            this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + pushedbackDir * Time.deltaTime);
            //Debug.Log("pushback is occurring: " + this.gameObject.GetComponent<Rigidbody>().position);
            
        }
    }

    public void GetDamage()
    {
        myHealth--;
		StartCoroutine(FlashingHurt());
		GameObject childGO = this.gameObject.transform.FindChild("FirstPersonCharacter").gameObject;
		childGO.GetComponent<AudioSource> ().Play ();
        isPushedback = true;
        StartCoroutine(WhenGetHurtAgain());

        if (myHealth == 1)
        {
			this.gameObject.transform.GetComponentInChildren<ProjectileShooter>().BiggerProjectile();
        }

        if (myHealth <= 0)
        {
            //should make pause come up but restart scene for now.
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Enemy")
        {

            //this.gameObject.GetComponent<Rigidbody> ().AddForce(new Vector3(0,0,-50), ForceMode.VelocityChange);
            //this.gameObject.GetComponent<Rigidbody>().AddForce(-Vector3.back *1000, ForceMode.Impulse);
            // Debug.Log("pushback is occurring: " + this.gameObject.GetComponent<Rigidbody>().velocity);
            //rigidbody.AddForce(transform.up * power, ForceMode.Impulse);
            //this.gameObject.GetComponent<Rigidbody>().set
            if (canGetHurt)
            {
                GetDamage();
                Debug.Log("get damage");
            }
            
            //play audio
            //Do damage to enemy
            //other.GetComponent<EnemyHealth>().GetDamage();
           // Destroy(this.gameObject.transform.parent.gameObject);
        }
    }

    IEnumerator WhenGetHurtAgain()
    {
        canGetHurt = false;
        

        yield return new WaitForSeconds(gracePeriod);
        canGetHurt = true;
        isPushedback = false;
        this.gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }

	IEnumerator FlashingHurt()
	{
		FlashRed.SetActive (true);


		yield return new WaitForSeconds(.2f);

		FlashRed.SetActive (false);

	}
}
