﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToNextLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player go to next level");
            //get current scene
            Scene currentScene = SceneManager.GetActiveScene();
            //Load scene that is after current scene by adding 1 to build index
            SceneManager.LoadScene(currentScene.buildIndex + 1);

        }
    }
}
