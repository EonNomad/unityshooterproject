﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDoorCounter : MonoBehaviour {
    //public float enemyDeaths;
    //public float DesiredEnemyDeaths;
    public int myIndex;

    public GameObject[] waves;

    public GameObject NextLevelTrigger;

    // Use this for initialization
    void Start () {
        myIndex = 0;
        Debug.Log("array length: " + waves.Length);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToNextWave()
    {
        myIndex++;
        //if (waves[myIndex] != null)
        if (myIndex + 1<= waves.Length)
        {
            waves[myIndex].SetActive(true);
        } else if(myIndex + 1 > waves.Length)
        {
            NextLevelTrigger.SetActive(true);
            Destroy(this.gameObject);
        }
        
    }
}
