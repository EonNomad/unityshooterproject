﻿using UnityEngine;
using System.Collections;

public class ProjectileShooter : MonoBehaviour {

	string FireProjectile;
	float numFire;
	bool m_Fire;
    bool m_FireTrig;
    bool m_OldFireTrig;

    public bool canFire;
    public float fireRate;
    public float SlowFireRate;

    GameObject prefab;
	void Start () {
        prefab = Resources.Load("Projectile2") as GameObject;
        //m_Fire = false;
        canFire = true;

		if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
		{
			FireProjectile = "Windows Fire";
		}

		if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
		{
			FireProjectile = "Mac Fire";
		}
		Debug.Log(FireProjectile);
	
	}
	
	// Update is called once per frame
	void Update () {

		// the jump state needs to read here to make sure it is not missed
		if (!m_Fire)
		{
			
			m_Fire = Input.GetButtonDown(FireProjectile);
		
			//m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");


		}

		//if (numFire<1f)
		//{
			if (Input.GetJoystickNames().Length > 0)
			{
				//needs something to tell trigger that button is let go
				numFire = 0f;
				m_Fire = false;

                m_FireTrig = Input.GetAxis(FireProjectile) > 0.0;
				//Debug.Log("numFire number: " + numFire);

			}
		//}

		if (!m_OldFireTrig == m_FireTrig) {
			m_Fire = true;
            m_OldFireTrig = m_FireTrig;


        } 
       
	
	}
    //I believe Fixed update happens after update and I was just following a similar set up for the FPController logic in the script
	void FixedUpdate()
	{
		//Debug.Log("is fixed update running? ");
		if(m_Fire){
            if (canFire)
            {
                //Debug.Log("Firing working" + FireProjectile);
                GameObject projectile = Instantiate(prefab) as GameObject;
				this.gameObject.GetComponent<AudioSource> ().Play ();
                projectile.transform.position = transform.position + Camera.main.transform.forward * 2;
                Rigidbody rb = projectile.GetComponent<Rigidbody>();
                rb.velocity = Camera.main.transform.forward * 40;
                
                m_Fire = false;
                if (this.gameObject.transform.parent.parent.GetComponent<PlayerHealth>().myHealth == 2)
                {
                    StartCoroutine(ControlFireRate(fireRate));
                }
				if (this.gameObject.transform.parent.parent.GetComponent<PlayerHealth>().myHealth == 1)
                {
                    StartCoroutine(ControlFireRate(SlowFireRate));
                }

            }
			
		}
	}

    IEnumerator ControlFireRate(float GrabbedRate)
    {
        canFire = false;
        yield return new WaitForSeconds(GrabbedRate);
        canFire = true;

    }

    public void BiggerProjectile()
    {
        prefab = Resources.Load("Projectile3") as GameObject;

    }
}
