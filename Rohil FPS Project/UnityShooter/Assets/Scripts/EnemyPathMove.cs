﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathMove : MonoBehaviour {

	 
	Transform target;
	public Transform waypoint;
	public Transform waypoint2;
	bool isMovingToWaypoint1;
    public bool isFry;
    public bool isSniper;
    public bool isFinalBoss;

    public float moveSpeed;

	float step;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		isMovingToWaypoint1 = true;
		//step = moveSpeed * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {
		//move towards
		MoveToWaypoint();

        if (isFry || isSniper)
        {
            //rotate towards
            Vector3 targetDir = target.position - transform.position;
            float step = moveSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
            Debug.DrawRay(transform.position, newDir, Color.red);
            transform.rotation = Quaternion.LookRotation(newDir);
        }

		
	}

	void MoveToWaypoint(){
		
		if(isMovingToWaypoint1){
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, waypoint.position, step);
		} else if(!isMovingToWaypoint1){
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, waypoint2.position, step);
		}

		if(transform.position == waypoint.transform.position){
			isMovingToWaypoint1 = false;
		}

		if(transform.position == waypoint2.transform.position){
			isMovingToWaypoint1 = true;
		}

	}
}
