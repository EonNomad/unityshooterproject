﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalBossShoot : MonoBehaviour {

    //GameObject prefab;
    public float fireRate;
    int myIndex;

    public GameObject[] projectiles;

    // Use this for initialization
    void Start()
    {

        //prefab = Resources.Load("HighRowProjectile") as GameObject;
        myIndex = 0;
        InvokeRepeating("LaunchProjectile", 2.0f, fireRate);
        StartCoroutine(SwitchProjectileWall());
    }


    void LaunchProjectile()
    {
        //Rigidbody instance = Instantiate(projectile);

        //instance.velocity = Random.insideUnitSphere * 5;

        GameObject wallShot = Instantiate(projectiles[myIndex]) as GameObject;

        wallShot.transform.position = transform.position + this.gameObject.transform.forward * 5;
        
        //projectile.transform.position = transform.position + (this.gameObject.transform.forward * 7) + this.gameObject.transform.up * 2;
        
        Rigidbody rb = wallShot.GetComponent<Rigidbody>();
        rb.velocity = this.gameObject.transform.forward * 200;
    }

    IEnumerator SwitchProjectileWall()
    {
        yield return new WaitForSeconds(5f);
        myIndex = Random.Range(0,3);
        StartCoroutine(SwitchProjectileWall());
    }


}
