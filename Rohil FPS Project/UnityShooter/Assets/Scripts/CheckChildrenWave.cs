﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckChildrenWave : MonoBehaviour {
    //public float CheckTime;
    public GameObject ExitDoor;
	// Use this for initialization
	void Start () {
        //InvokeRepeating("CheckIfChildrenGone", 3.0f, CheckTime);
    }
	
	// Update is called once per frame
	void Update () {
        CheckIfChildrenGone();

    }

    void CheckIfChildrenGone()
    {
        if (this.gameObject.transform.childCount == 0)
        {

            ExitDoor.GetComponent<UnlockDoorCounter>().GoToNextWave();
            //Destroy(this.gameObject);
            this.gameObject.GetComponent<CheckChildrenWave>().enabled = false;
        }
    }
}
