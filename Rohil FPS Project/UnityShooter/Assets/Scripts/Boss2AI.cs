﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2AI : MonoBehaviour {

    Transform target;

    public float moveSpeed;
    public bool canFollow;
    public bool canEvade;

    public float cantEvadeDur;
    Vector3 leftDir;
    Vector3 rightDir;
    Vector3 randomDirection;

    GameObject burgerSpawn;
    public float spawnRate;

    // Use this for initialization
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        canFollow = true;
        canEvade = true;

        leftDir = new Vector3(-30, 0, 0);
        rightDir = new Vector3(30, 0, 0);
        randomDirection = leftDir;

        StartCoroutine(SwitchDirectionRight());

        burgerSpawn = Resources.Load("Burger") as GameObject;
        InvokeRepeating("SpawnBurgerEnemy", 5.0f, spawnRate);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canEvade)
        {
            Vector2 mouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Ray ray;
            ray = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.rigidbody == this.gameObject.GetComponent<Rigidbody>())
                {
                    float step = moveSpeed * Time.deltaTime;
                    canFollow = false;
                    //Evade

                    //transform.position = Vector3.left;
                    //Vector3 randomDirection = new Vector3(Random.Range(-100,100), 0, 0);
                    //transform.position = transform.position+ randomDirection;
                    this.gameObject.GetComponent<Rigidbody>().MovePosition(transform.position + randomDirection * Time.deltaTime);
                    StartCoroutine(EvadeCooldown());
                    Debug.Log("detected boss");
                }
            }
        }


        if (canFollow)
        {
            //move towards
            GoToPlayer();
        }
        FacePlayer();

    }

    void GoToPlayer()
    {
        float step = moveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    void FacePlayer()
    {
        float step = moveSpeed * Time.deltaTime;

        //rotate towards
        Vector3 targetDir = target.position - transform.position;
        //float step = moveSpeed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);
    }

    IEnumerator EvadeCooldown()
    {
        yield return new WaitForSeconds(2f);
        canEvade = false;
        canFollow = true;
        yield return new WaitForSeconds(cantEvadeDur);
        canEvade = true;
    }

    IEnumerator SwitchDirectionRight()
    {
        yield return new WaitForSeconds(5f);
        randomDirection = rightDir;
        StartCoroutine(SwitchDirectionLeft());
    }

    IEnumerator SwitchDirectionLeft()
    {
        yield return new WaitForSeconds(5f);
        randomDirection = leftDir;
        StartCoroutine(SwitchDirectionRight());
    }

    void SpawnBurgerEnemy()
    {
        //Rigidbody instance = Instantiate(projectile);

        //instance.velocity = Random.insideUnitSphere * 5;

        GameObject spawnThis = Instantiate(burgerSpawn) as GameObject;

        // spawnThis.transform.position = transform.position + this.gameObject.transform.forward * 7;

        spawnThis.transform.position = transform.position + (this.gameObject.transform.forward * 7) + this.gameObject.transform.up * 2;
        
        //Rigidbody rb = projectile.GetComponent<Rigidbody>();
        //rb.velocity = this.gameObject.transform.forward * 40;
    }
}
