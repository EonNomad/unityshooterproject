﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            //Debug.Log("Hit Enemy");
            //play audio
            //Do damage to enemy
            //other.GetComponent<EnemyHealth>().GetDamage();
            Destroy(this.gameObject.transform.parent.gameObject);
        }

        if (other.gameObject.tag == "Solid")
        {
            //Debug.Log("Hit something");
            //
            //play audio
            Destroy(this.gameObject.transform.parent.gameObject);
        }

    }
}
