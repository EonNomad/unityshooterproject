﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDmg : MonoBehaviour {



    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("Hit Enemy");
            //play audio
            //if enemy hit has health script (enemy and not bullet with enemy tag)
            if (other.GetComponent<EnemyHealth>()!=null)
            {
                //Do damage to enemy
                other.GetComponent<EnemyHealth>().GetDamage();
                //destroy this bullet
                Destroy(this.gameObject.transform.parent.gameObject);
            }
            
        }

        if (other.gameObject.tag == "Solid")
        {
            Debug.Log("Hit something");
            //
            //play audio
			//this.gameObject.GetComponent<AudioSource>().Play();

			Destroy(this.gameObject.transform.parent.gameObject);
			
            
        }
       
    }
}
